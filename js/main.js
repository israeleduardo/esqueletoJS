require.config({

    baseUrl: "js/scripts",
    
	// alias libraries paths
    paths: {
        'angular': '../lib/angular/angular',
        'angular-route': '../lib/angular/angular-route',
        'async': '../lib/requirejs/async',
        'angularAMD': '../lib/requirejs/angularAMD',
        'ngload': '../lib/requirejs/ngload',
        'ui-bootstrap': '../lib/angular-ui-bootstrap/ui-bootstrap-tpls-0.13.0',
        'prettify': '../lib/google-code-prettify/prettify',


        "jquery":  "../lib/jquery/jquery-1.11.3.min",
        "bootstrap": "../lib/bootstrap/bootstrap.min",
        "inputmask": "../lib/jquery/jquery.inputmask",
        "moment":  "../lib/moment/moment",
        "jspdf":   "../lib/jspdf/jspdf.min",


        'HomeController': 'controller/home_ctrl',
        'MapController': 'controller/map_ctrl',
        'ModulesController': 'controller/modules_ctrl'
    },

    // Add angular modules that does not support AMD out of the box, put it in a shim
    shim: {
        'angularAMD': ['angular'],
        'angular-route': ['angular']
    },

    // kick start application
    deps: ['app']
});
